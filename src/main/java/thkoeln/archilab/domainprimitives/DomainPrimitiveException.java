package thkoeln.archilab.domainprimitives;

public class DomainPrimitiveException extends RuntimeException {
    public DomainPrimitiveException(String message) {
        super(message);
    }
}
