package thkoeln.archilab.domainprimitives;

/**
 * Simple implementation if 13-digit ISBN numbers as a domain primitive.
 * Rules for ISBN-13 can be found e.g. here:
 *
 */
public class ISBNNumber13 {
    private final Integer[] isbnDigits;
    private static final int LENGTH = 13;
    private static final int CHECKDIGIT_INDEX = 12;

    /**
     * Factory method to create an ISBN-13 number from a string.
     * The string may contain hyphens or spaces (a simplified version of an ISBN-13 number).
     * @param isbnString
     * @return
     */
    public static ISBNNumber13 of( String isbnString ) {
        return new ISBNNumber13( isbnString );
    }


    private ISBNNumber13( String isbnString ) {
        isbnDigits = new Integer[LENGTH];
        parseIsbnString( isbnString );
        assertProperCheckDigit();
    }

    /**
     * The ISBN-13 number is stored as an array of integers.
     * The string representation may contain a lot of extra characters.
     * For simplicity, we just assume that the string contains only digits and either hyphens or spaces.
     * @param isbnString
     */
    private void parseIsbnString( String isbnString ) {
        throw new UnsupportedOperationException( "Not yet implemented" );
    }

    /**
     * The check digit calculation can be found e.g. here:
     * https://isbn-information.com/check-digit-for-the-13-digit-isbn.html
     *
     * @throws DomainPrimitiveException if the check digit is not correct
     */
    private void assertProperCheckDigit() {
        throw new UnsupportedOperationException( "Not yet implemented" );
    }
}
