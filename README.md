# Domain Primitive Example - ISBN Number

ISBN numbers are a good example for a domain primitive. They are a unique identifier for a book 
and are used in many different contexts. They are also a good example of a domain primitive 
because they are immutable and have a well-defined format. 

The check digit at the end of the ISBN number is used to verify that the number is correct. 
This makes it easy to validate an ISBN number and ensure that it is correct. The check digit
is calculated using a simple formula that is easy to implement in code - a very good example
of what you would like to encapsulate in a domain primitive.

## About this repository

This repo is supposed to give an example of how to implement a domain primitive, here for 
ISBN numbers. At the same time, it is supposed to show how AI tools like ChatGPT and GitHub
Copilot can help you write code.

The development is supposed to be (somewhat) test-driven. Therefore, we'll first write the 
tests, then the implementation.

The "empty" implementation of the ISBN number, and an empty test file, are in the main branch, 
if you want to try it yourself. The solution is in the `solution` branch. 


## Assumptions 

We use the following information & assumptions as a basis for our implementation. 
* We only model the newer ISBN-13 numbers, not the older ISBN-10 numbers.
* The ISBN-13 number consists of 13 digits, the last one being the check digit.
* We assume that this string reprensentation that we get may contain digits, hyphens, and spaces.
  (The reality is a little more complex, but we'll keep it simple for now.)
* The check digit is calculated using the following formula (taken)
  (from [this source](https://isbn-information.com/the-13-digit-isbn.html), but there are many others):
  * Take the first 12 digits of the ISBN number.
  * Multiply each digit by a weight, alternating between 1 and 3.
  * Sum up the results.
  * Take the sum modulo 10.
  * Subtract the result from 10.
  * The result is the check digit.


## Approach, using AI as a helper   

### Let ChatGPT create test data 

[In this website](https://www.maryruthbooks.com/teaching-resources/leveled-book-lists/isbn/), we
find lists of valid ISBN numbers for children's books. We can use these to create test data. We can 
ask ChatGPT transform this website into a list of valid ISBN numbers. We can also ask it to
build in variations of hyphens and spaces.

In addition, we can ask ChatGPT to generate invalid ISBN numbers, by changing the check digit in the
above list.

### Let Copilot help with writing the tests

Copilot can write the tests for us, based on the above test data, with `assertThrows` and 
`assertNotThrows` clauses. 

In addition, we can "specify" additional tests with null inputs, invalid characters, or wrong number
of digits, just by the file name. Let's see if Copilot properly guesses our intentions.


### Let ChatGPT and Copilot help with the implementation

We can ask ChatGPT to implement the ISBN number class, based on the tests. 

And let's try the same with Copilot.
